/* debug.h
 * Debug macros go here
 */

#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG_INCOMING_PACKET 0
#define DEBUG_OUTGOING_PACKET 0
#define DEBUG_INCOMPLETE_PACKET 0
#define DEBUG_PRINT_LOCAL_PORT 1
#define DEBUG_PRINT_PUBLIC_IP 1
#define DEBUG_PREVIOUS_PACKET_MISSING 0
#define DEBUG_PRINT_AUTHENTICATION_MESSAGE 1

/* Usage: debugCout(1<<"hello"<<' '<<"world"); */
#ifdef DEBUG
#define debugCout(str) do { std::cout << "[DEBUG] " << str << std::endl; } while( false )
#else
#define debugCout(str) do { } while ( false )
#endif

/* Usage: debugFlagCout(DEBUG | FLAG_LOG_CONNECT, "connected: "<<variable<<" status:"<<status) */
#ifdef DEBUG
#define debugFlagsCout(flags, str) do { if(flags) { std::cout << "[DEBUG] " << str << std::endl; } } while( false )
#else
#define debugFlagsCout(flags, str) do { } while ( false )
#endif


#endif
 
