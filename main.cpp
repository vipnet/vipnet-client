#include "client.h"

/* Parse args to set protocol number and protocl specific information */
void parseArgs(Client &client, int argc, char **argv) {
	assert(argc >= 8 
		&& "Usage: ./client <server_ip> <server_port> <if_name> <if_ip> <if_mask> <udp/tcp> <protocol_number> "
		   "[protocol_specific_info]");

	uint8_t protocolNumber = atoi(argv[7]);
	if(std::string(argv[6]) == "tcp") {
		assert (protocolNumber == 0x00 && "Protocol number invalid");
	}
	else {
		assert( (protocolNumber == 0x00 || protocolNumber == 0x01) && "Protocol number invalid.");
		int port = 0;

		/* Protocol 0 => Port may or may not be given */
		if(protocolNumber == 0x00) {
			if(argc == 9)
				port = atoi(argv[8]);
		}
		/* Protocol 1 => IP is guaranteed, Port may or may not be given */
		else if(protocolNumber == 0x01) {
			assert(argc >= 9 && "Expected public IP and optional public port as protocol specific info");
			std::string publicIp = std::string(argv[8]);
			client = client.setPublicIp(publicIp);

			if(argc == 10)
				port = atoi(argv[9]);
		}
		client = client.setPublicPort(port);
	}
	client = client.setProtocolNumber((uint8_t)protocolNumber)
				   .setConnectionMethod(argv[6]);
}

int main(int argc, char *argv[]) {
	Client client = Client::getInstance()
				.setServerIP(std::string(argv[1]))
				.setServerPort(atoi(argv[2]))
				.setDevName(std::string(argv[3]))
				.setInterfaceIP(std::string(argv[4]))
				.setInterfaceMask(std::string(argv[5]));

	parseArgs(client, argc, argv);
	client.runClient();

	return 0;
}
