#include "client.h"

Client& Client::setServerIP(std::string serverIP) {
	this->serverIP = serverIP;
	return *this;
}

Client& Client::setServerPort(int serverPort) {
	this->serverPort = serverPort;
	return *this;
}

Client& Client::setDevName(std::string devName) {
	this->devName = devName;
	return *this;
}

Client& Client::setInterfaceIP(std::string IP) {
	this->interfaceIP = IP;
	return *this;
}

Client& Client::setInterfaceMask(std::string mask) {
	this->interfaceMask = mask;
	return *this;
}

Client& Client::setProtocolNumber(uint8_t protocolNumber) {
	this->protocolNumber = protocolNumber;
	return *this;
} 

Client& Client::setPublicIp(std::string publicIp) {
	this->publicIp = publicIp;
	return *this;
}

Client& Client::setPublicPort(int port) {
	this->UDP_listeningPort = port;
	return *this;
}


Client& Client::setConnectionMethod(std::string connectionMethod) {
	assert((connectionMethod == "udp" || connectionMethod == "tcp") && "Connection method should be 'udp' or 'tcp'");
	this->connectionMethod = connectionMethod;

	if(connectionMethod == "tcp") {
		this->initialPacketSize = this->TCP_initialPacketSize[this->protocolNumber];
		this->encapsulationSize = this->TCP_encapsulationSize;
	} else {
		this->initialPacketSize = this->UDP_initialPacketSize[this->protocolNumber];
		this->encapsulationSize = this->UDP_encapsulationSize;
	}

	/* 1500-44 (UDP) or 1500-56 (TCP) */
	this->MTUSize = this->DEFAULT_MTU_SIZE - this->encapsulationSize;

	return *this;
}

int Client::createTapInterface() {
	struct ifreq interfaceRequest;
	int tapDevFd, socketFd;
	std::string tapDevPath = "/dev/net/tun";
	struct sockaddr_in *addr;

	assert(this->devName.size() > 0 && "Wrong interface name!");

	/* Open the Linux "special file" virtual interface */
	tapDevFd = open(tapDevPath.c_str() , O_RDWR);
	assert(tapDevFd && "Opening /dev/net/tun");

	memset(&interfaceRequest, 0, sizeof(struct ifreq));
	strncpy(interfaceRequest.ifr_name, this->devName.c_str(), IFNAMSIZ);

	/* Create a TAP Interface*/
	interfaceRequest.ifr_flags = IFF_TAP | IFF_NO_PI;
	auto returnCode = ioctl(tapDevFd, TUNSETIFF, &interfaceRequest);
	assert(returnCode != -1 && "ioctl(TUNSETIFF)");
	/* Create a socket that will be used for ioctl to set the MTU, IP, mask and interface UP */
	socketFd = socket(AF_INET, SOCK_DGRAM, 0);
	assert(socketFd != -1 && "socket");
	/* Get the IFs flags for the socket */
	returnCode = ioctl(socketFd, SIOCGIFFLAGS, &interfaceRequest);
	assert(returnCode != -1 && "ioctl");
	/* Set the MTU as specified in the header (default 1500-encapsulation headers) */
	interfaceRequest.ifr_mtu = this->MTUSize;
	returnCode = ioctl(socketFd, SIOCSIFMTU, &interfaceRequest);
	assert(returnCode != -1 && "ioctl");
	/* Set the interface up */
	interfaceRequest.ifr_flags = IFF_UP;
	returnCode = ioctl(socketFd, SIOCSIFFLAGS, &interfaceRequest);
	assert(returnCode != -1 && "ioctl(SIOCSIFFLAGS)");

	/* IP */
	addr = (struct sockaddr_in *)&interfaceRequest.ifr_addr;
	addr->sin_family = AF_INET;
	addr->sin_port = 0;
 	returnCode = inet_pton(AF_INET, this->interfaceIP.c_str(), &addr->sin_addr);
	assert(returnCode != -1 && "inet_pton");
	returnCode = ioctl(socketFd, SIOCSIFADDR, &interfaceRequest);
	assert(returnCode != -1 && "ioctl(SIOCSIFADDR)");
	/* Network Mask */
 	returnCode = inet_pton(AF_INET, this->interfaceMask.c_str(), &addr->sin_addr);
	assert(returnCode != -1 && "inet_pton");
	returnCode = ioctl(socketFd, SIOCSIFNETMASK, &interfaceRequest);
	assert(returnCode != -1 && "ioctl(SIOCSIFNETMASK)");

	return tapDevFd;
}

void Client::setTapInterfaceMAC() {
	struct ifreq if_buf;

	memset(&if_buf, 0, sizeof(if_buf));
	auto result = ioctl(this->tapFd, SIOCGIFHWADDR, &if_buf);
	assert(result != -1);
	memcpy(this->tapMac, if_buf.ifr_hwaddr.sa_data, 6);
}

void Client::setRemoteSockAddr() {
	memset(&this->remoteSockAddr, 0, sizeof(struct sockaddr_in));
	this->remoteSockAddr.sin_family = AF_INET;
	this->remoteSockAddr.sin_addr.s_addr = inet_addr(this->serverIP.c_str());
	this->remoteSockAddr.sin_port = htons(this->serverPort);
	this->remoteSockAddrSize = sizeof(this->remoteSockAddr);
}

int Client::TCP_connectToServer() {
	this->serverFd = socket(AF_INET, SOCK_STREAM, 0);
	assert(this->serverFd >= 0 && "socket");
	auto returnCode = connect(this->serverFd, (struct sockaddr*) &this->remoteSockAddr, sizeof(struct sockaddr_in));
	assert(returnCode != -1 && "connect");

	return this->serverFd;
}

int Client::UDP_createListeningSocket() {
	struct sockaddr_in localSockAddr;
	socklen_t szSockAddrIn = sizeof(struct sockaddr_in);

	this->serverFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	assert(this->serverFd != -1 && "Couldn't create socket.");

	/* Set the local structure for the socket*/
	memset(&localSockAddr, 0, szSockAddrIn);
	localSockAddr.sin_family = AF_INET;
	localSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	localSockAddr.sin_port = this->UDP_listeningPort;

	/* Create a listening socket */
	auto returnCode = bind(this->serverFd, (struct sockaddr *)&localSockAddr, szSockAddrIn);
	assert(returnCode != -1 && "Couldn't bind socket to local address/port");

	/* If no default port was selected, the OS generated one, so we must save it. */
	if(this->UDP_listeningPort == 0){
		auto returnCode = getsockname(this->serverFd, (struct sockaddr *)&localSockAddr, &szSockAddrIn);
		assert(returnCode != -1 && "getsockname");
		this->UDP_listeningPort = htons(localSockAddr.sin_port);
	}

	return this->serverFd;
}

void Client::sendInitialPacket() {
	char buffer[BUFSIZE];
	uint16_t bufferSize = htons(this->initialPacketSize);
	in_addr_t publicIp;

	memcpy(&buffer[0], &bufferSize, 2);
	buffer[2] = 0xAA;
	buffer[3] = 0xBB;
	buffer[4] = 0xCC;
	buffer[5] = 0xDD;
	buffer[6] = 0xEE;
	buffer[7] = 0xFF;
	memcpy(&buffer[8], this->tapMac, 6);
	buffer[14] = this->protocolNumber;
	if(this->connectionMethod == "udp"){
		if(this->protocolNumber == 0x00) {
			memcpy(&buffer[15], &this->UDP_listeningPort, 2);
		}
		else if(this->protocolNumber == 0x01) {
			publicIp = inet_addr(this->publicIp.c_str());
			memcpy(&buffer[15], &publicIp, 4);
			memcpy(&buffer[19], &this->UDP_listeningPort, 2);
		}
	}
	buffer[this->initialPacketSize + sizeof(uint16_t)] = 0;

	/* Send the authentication message */
	auto returnCode = this->writePacket(this->serverFd, buffer, this->initialPacketSize + sizeof(uint16_t));
	assert((unsigned int)returnCode == this->initialPacketSize + sizeof(uint16_t) && "writePacket");

	/* Read the server's response */
	returnCode = this->readPacket(this->serverFd, buffer, BUFSIZE);
	assert(returnCode > 0 && "readPacket");
	debugFlagsCout(DEBUG_PRINT_AUTHENTICATION_MESSAGE, "Received from server:" << buffer);
	assert(strncmp(buffer, ACCEPT, strlen(ACCEPT)) == 0);
}

inline int Client::writePacket(int socket, char *buffer, int size) {
	if(this->connectionMethod == "tcp")
		return write(socket, buffer, size);
	else
		return sendto(socket, buffer, size, 0, (struct sockaddr *)&this->remoteSockAddr, sizeof(sockaddr_in));
}

inline int Client::readPacket(int socket, char *buffer, int size) {
	if(this->connectionMethod == "tcp")
		return read(socket, buffer, size);
	else
		return recvfrom(socket, buffer, size, 0, (struct sockaddr *)&this->remoteSockAddr, &this->remoteSockAddrSize);
}

void Client::runClient() {
	int maxFd, returnCode, nRead, nWrite;
	fd_set readSet;
	char buffer[BUFSIZE];

	this->tapFd = this->createTapInterface();
	assert(this->tapFd != -1 && "Error creating the tap interface.");
	this->setTapInterfaceMAC();
	this->setRemoteSockAddr();

	if(this->connectionMethod == "udp") {
		this->serverFd = this->UDP_createListeningSocket();
		assert(this->serverFd != -1 && this->UDP_listeningPort > 0 && "Error creating the local UDP socket");
		debugFlagsCout(DEBUG_PRINT_LOCAL_PORT, "Local port = " << this->UDP_listeningPort);
		debugFlagsCout(DEBUG_PRINT_PUBLIC_IP && this->protocolNumber == 0x01, "Public IP = " << this->publicIp);
	}
	else {
		this->serverFd = this->TCP_connectToServer();
	}

	this->sendInitialPacket();
	debugFlagsCout(DEBUG_PRINT_LOCAL_PORT, "Client is now connected to the server.");

	maxFd = std::max(this->tapFd, this->serverFd);
	while(true) {
		FD_ZERO(&readSet);
		FD_SET(this->tapFd, &readSet);
		FD_SET(this->serverFd, &readSet);
		memset(buffer, 0, BUFSIZE);
		
		returnCode = select(maxFd + 1, &readSet, NULL, NULL, NULL);
		if(returnCode < 0) {
			if(errno == EINTR)
				continue;
			assert(0 && "select");
		}

		if(FD_ISSET(this->tapFd, &readSet)) {
			/* Received a packet on virtual interface. Just read it and write it to the server. */
			nRead = read(this->tapFd, &buffer[sizeof(uint16_t)], BUFSIZE - sizeof(uint16_t));
			assert(nRead != -1 && "read");

			/* Put the length at the begining of the buffer as a network packet */
			*(uint16_t *)buffer = htons(nRead);
			debugFlagsCout(DEBUG_OUTGOING_PACKET, "-> Sending a packet to server. Size:" << nRead << " (+2) bytes");

			/* Write length + packet as a whole to the vpn server */
			returnCode = this->writePacket(this->serverFd, buffer, nRead + sizeof(uint16_t));
			assert(returnCode != -1 && "write");
		}

		if(FD_ISSET(this->serverFd, &readSet)) {
			/* Receive a packet from the vpn server */	
			nRead = this->readPacket(this->serverFd, buffer, BUFSIZE);
			if(nRead == 0)
				continue;

			debugFlagsCout(DEBUG_INCOMING_PACKET, "<- Received a packet from the server. Size: " << nRead
				<< " (+2) bytes");
			
			/* If we have some unread packet from previous packet, we must first write that before proceeding to the 
			 * next potentially chained packets
			 */
			int offsetRead = 0, packetSize = 0;
			if (this->previousPacketMissing > 0) {
				assert(this->previousPacket);
				debugFlagsCout(DEBUG_PREVIOUS_PACKET_MISSING, "Previous packet missing = " 
					<< this->previousPacketMissing << " Previous packet size = " << this->previousPacketSize 
					<< " nRead = " << nRead);

				/* If the packet is split into even more receives, just copy this intermediate receive and continue,
				 * becuase no aditional packet was received. All data is partial data from that one unfinished packet */
				if (nRead < this->previousPacketMissing) {
					this->previousPacketMissing -= nRead;
					memcpy(&this->previousPacket[this->previousPacketSize-this->previousPacketMissing], buffer, nRead);
					continue;
				}
				else {
					memcpy(&this->previousPacket[this->previousPacketSize-this->previousPacketMissing], buffer, 
						this->previousPacketMissing);
				}

				nWrite = write(this->tapFd, this->previousPacket, this->previousPacketSize);
				if(nWrite != this->previousPacketSize)
					printf("Warning nWrite = %i previousPacketSize = %i\n", nWrite, this->previousPacketSize);
				offsetRead += this->previousPacketMissing;
				this->previousPacketMissing = 0;
				this->previousPacketSize = 0;
				delete this->previousPacket;
				this->previousPacket = NULL;
			}

			while(offsetRead < nRead) {
				packetSize = ntohs(*(uint16_t *)&buffer[offsetRead]);
				if(packetSize == 0) {
					printf("Warning, packet size = 0 at offsetRead=%i out of nRead=%i\n", offsetRead, nRead);
					break;
				}

				debugFlagsCout(DEBUG_INCOMING_PACKET, "| <- offsetRead = " << offsetRead  << " packetSize = "
					<< packetSize);
				
				if(offsetRead + sizeof(uint16_t) + packetSize > BUFSIZE) {
					/* BUFSIZE = 1500, offset = 1300, packetSize = 500 =>
							previousPacketMissing = 500 - (1500 - 1300) = 300
					 * So, we'll need to copy first 500-200=300 bytes from the next packet. */
					int existentPacketSize =  BUFSIZE - offsetRead - sizeof(uint16_t);
					this->previousPacketMissing = packetSize - existentPacketSize;
					this->previousPacketSize = packetSize;
					this->previousPacket = new char[packetSize];
					assert(this->previousPacket && "Error at allocating buffer for prev packet");
					memcpy(this->previousPacket, &buffer[offsetRead + sizeof(uint16_t)], existentPacketSize);
					debugFlagsCout(DEBUG_INCOMPLETE_PACKET, "Warning, packet may be incomplete, expecting next one "
						<< "to finish it. offsetRead=" << offsetRead << "+packetSize=" << packetSize << "+len=2 > "
						<< "BUFSIZE=" << BUFSIZE << ". Stored="<<existentPacketSize<<" Left=" 
						<< this->previousPacketMissing);
					break;
				}

				nWrite = write(this->tapFd, &buffer[offsetRead + sizeof(uint16_t)], packetSize);
				if(nWrite != packetSize)
					printf("Warning nWrite = %i packetSize = %i\n", nWrite, packetSize);

				offsetRead += packetSize + sizeof(uint16_t);
			}
		}
	}
}