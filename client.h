#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>
#include <netinet/if_ether.h>
#include <netinet/udp.h>
#include <netinet/ip.h>
#include <net/if.h>

#include <iostream>
#include <assert.h>
#include <string>

#include "debug.h"

class Client {
public:
	/* Singleton */
	static Client& getInstance() {
		static Client instance;
		return instance;
	}
	//Client(Client const&) = delete;
	//void operator=(Client const&) = delete;
	~Client() {};

	/* Builder setters */
	Client& setServerIP(std::string serverIP);
	Client& setServerPort(int serverPort);
	Client& setDevName(std::string devName);
	Client& setInterfaceIP(std::string IP);
	Client& setInterfaceMask(std::string mask);
	Client& setConnectionMethod(std::string connectionType);
	Client& setProtocolNumber(uint8_t protocolNumber);
	Client& setPublicIp(std::string publicIp);
	Client& setPublicPort(int port);

	/** Getters and setters */
	void setRemoteSockAddr();
	void setTapInterfaceMAC();
	
	/* Methods */
	void runClient();
	int createTapInterface();
	void sendInitialPacket();

	/* connectToServer connects to server, while createListeningUDPSocket adds the desired send port for the server */
	int TCP_connectToServer();
	int UDP_createListeningSocket();
	inline int writePacket(int socket, char *buffer, int size);
	inline int readPacket(int socket, char *buffer, int size);

	/* Constants */
	static const int TCP_encapsulationSize = 56, UDP_encapsulationSize = 44;
	static const unsigned int BUFSIZE = 1500, DEFAULT_MTU_SIZE = 1500;
	const char *ACCEPT = "ACCEPT";
	const char *REJECT = "REJECT";

private:
	Client():TCP_initialPacketSize{13}, UDP_initialPacketSize{15, 19} {};
	
	/* Server variables */
	struct sockaddr_in remoteSockAddr;
	socklen_t remoteSockAddrSize;
	int serverPort = -1, serverFd = -1;
	std::string serverIP;
	/* Interface variables */
	char tapMac[6];
	int tapFd = -1;
	std::string devName, interfaceIP, interfaceMask;
	/* Previous packet variables */
	int previousPacketMissing = -1, previousPacketSize = -1;
	char *previousPacket = NULL;

	/* TCP vs UDP variables */
	std::string connectionMethod; /* udp or tcp */
	uint8_t protocolNumber; /* protocol index */
	int initialPacketSize = -1, encapsulationSize = -1, MTUSize = -1;
	int UDP_listeningPort = 0;
	std::string publicIp;
	int TCP_initialPacketSize[1]; /* Array for packet sizes of different protocols */
	int UDP_initialPacketSize[2];
	
};