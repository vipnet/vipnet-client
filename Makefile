FLAGS = -Wall -Werror -g -std=c++11 -DDEBUG=1

all:
	g++ $(FLAGS) *.cpp -o client

clean:
	rm client
